﻿using System.Collections.Generic;

namespace PagoDataAcces.Interfaces
{
    public interface ICRUDBase<T>
    {
        string Create(T obj);
        List<T> GetByParameters(T obj);
        T GetById(int key);
        string Update(T obj);
        string Delete(int key);
    }
}
