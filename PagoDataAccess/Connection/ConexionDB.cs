﻿using MySql.Data.MySqlClient;

namespace PagoDataAccess.Conexion
{
    public class ConexionDB
    {
        public static MySqlConnection ConexionDBase()
        {
            string connStr = "server=localhost;user=root;database=pagodb;password=root;";
            MySqlConnection conn = new MySqlConnection(connStr);
            return conn;
        }
    }
}