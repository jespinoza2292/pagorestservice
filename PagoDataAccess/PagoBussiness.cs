﻿using MySql.Data.MySqlClient;
using PagoDataAcces.Interfaces;
using PagoDataAccess.Common;
using PagoDataAccess.Conexion;
using PagoDataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagoDataAccess
{
    public class PagoBussiness : ICRUDBase<PagoEntity>
    {
        public string Create(PagoEntity obj)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                PagoEntity oPago = new PagoEntity();
                var resultado = "0";
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_INSERT_PAGO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@descripcion", obj.descripcion);
                    cmd.Parameters.AddWithValue("@tipo", obj.tipo);
                    cmd.Parameters.AddWithValue("@codUsario", obj.codUsario);
                    cmd.Parameters.AddWithValue("@monto", obj.monto);

                    //abrir la conexion
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultado = "1";
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return resultado;
            }
        }

        public string Delete(int key)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                var resultado = "0";
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_DELETE_PAGO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idPago", key);

                    //abrir la conexion
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultado = "1";
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return resultado;
            }
        }

        public PagoEntity GetById(int key)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                PagoEntity oPago = new PagoEntity();
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_GET_PAGO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idPago", key);

                    //abrir la conexion
                    cmd.Connection.Open();

                    MySql.Data.MySqlClient.MySqlDataReader rd = cmd.ExecuteReader();
                    //ejecutar el query                    
                    while (rd.Read())
                    {
                        oPago.descripcion = rd.GetString("DESCRIPCION");
                        oPago.tipo = rd.GetString("TIPO");
                        oPago.fechaRegistro = rd.GetDateTime("FECHA_REGISTRO");
                        oPago.diaMesRegistro = rd.GetInt32("DIA_MES_INGRESO");
                        oPago.estado = rd.GetString("ESTADO");
                        oPago.monto = rd.GetDecimal("MONTO");
                        oPago.codUsario = rd.GetInt32("COD_USUARIO");
                        oPago.codMes = rd.GetString("COD_MES");
                        oPago.descripcionMes = rd.GetString("DESCRIPCION_MES");
                        oPago.fechaBaja = ClsCommon.getDate(rd["FECHA_BAJA"]);
                    }
                    return oPago;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }

        public List<PagoEntity> GetByParameters(PagoEntity obj)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                PagoEntity oPago;
                var dtInit = new DateTime();
                List<PagoEntity> listPago = new List<PagoEntity>();
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_LIST_PAGOS";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idUsuario", obj.codUsario);
                    if (obj.fechaDesde == dtInit) cmd.Parameters.AddWithValue("@fechaDesde", null);
                    else cmd.Parameters.AddWithValue("@fechaDesde", obj.fechaDesde);
                    if (obj.fechaHasta == dtInit) cmd.Parameters.AddWithValue("@fechaHasta", null);
                    else cmd.Parameters.AddWithValue("@fechaHasta", obj.fechaHasta);

                    //abrir la conexion
                    cmd.Connection.Open();

                    MySql.Data.MySqlClient.MySqlDataAdapter adr = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
                    DataTable dtData = new DataTable();
                    adr.Fill(dtData);
                    foreach (DataRow dr in dtData.Rows)
                    {
                        oPago = new PagoEntity();
                        oPago.idPago = ClsCommon.getInt32(dr["ID_PAGO"]);
                        oPago.descripcion = ClsCommon.getString(dr["DESCRIPCION"]);
                        oPago.tipo = ClsCommon.getString(dr["TIPO"]);
                        oPago.fechaRegistro = ClsCommon.getDate(dr["FECHA_REGISTRO"]);
                        oPago.diaMesRegistro = ClsCommon.getInt32(dr["DIA_MES_INGRESO"]);
                        oPago.estado = ClsCommon.getString(dr["ESTADO"]);
                        oPago.codUsario = ClsCommon.getInt32(dr["COD_USUARIO"]);
                        oPago.codMes = ClsCommon.getString(dr["COD_MES"]);
                        oPago.descripcionMes = ClsCommon.getString(dr["DESCRIPCION_MES"]);
                        oPago.fechaBaja = ClsCommon.getDate(dr["FECHA_BAJA"]);
                        listPago.Add(oPago);
                    }
                    return listPago;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }

        public string Update(PagoEntity obj)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                PagoEntity oPago = new PagoEntity();
                var resultado = "0";
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_UPDATE_PAGO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idPago", obj.idPago);
                    cmd.Parameters.AddWithValue("@descripcion", obj.descripcion);
                    cmd.Parameters.AddWithValue("@tipo", obj.tipo);
                    cmd.Parameters.AddWithValue("@monto", obj.monto);

                    //abrir la conexion
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultado = "1";
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return resultado;
            }
        }
    }
}
