﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagoDataAccess.Common
{
    public class ClsCommon
    {
        public static DateTime getDate(object attribute)
        {
            if (attribute != null)
            {
                if (attribute is DateTime)
                {
                    return Convert.ToDateTime(attribute);
                }
            }
            return new DateTime();
        }

        public static Int32 getInt32(object attribute) {
            if (attribute != null) {
                if (attribute is Int32) {
                    return Convert.ToInt32(attribute);
                }
            }
            return new Int32();
        }

        public static String getString(object attribute) {
            String strReturn = String.Empty;
            if (attribute != null)
            {
                if (attribute is String)
                {
                    return Convert.ToString(attribute);
                }
            }
            return strReturn;
        }
    }
}
