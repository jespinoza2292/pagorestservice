﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagoDataAccess.Entity
{
    public class IngresoEntity
    {
        public int idIngreso { get; set; }
        public string descripcion { get; set; }
        public string tipo { get; set; }
        public DateTime fechaRegistro { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
        public decimal monto { get; set; }
        public int diaMesRegistro { get; set; }
        public string estado { get; set; }
        public int codUsario { get; set; }
        public string codMes { get; set; }
        public string descripcionMes { get; set; }
        public DateTime fechaBaja { get; set; }
    }
}
