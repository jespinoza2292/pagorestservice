﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using PagoDataAcces.Interfaces;
using PagoDataAccess.Common;
using PagoDataAccess.Conexion;
using PagoDataAccess.Entity;

namespace PagoDataAccess
{
    public class IngresoBussiness : ICRUDBase<IngresoEntity>
    {
        public string Create(IngresoEntity obj)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                IngresoEntity oIngreso = new IngresoEntity();
                var resultado = "0";
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_INSERT_INGRESO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@descripcion", obj.descripcion);
                    cmd.Parameters.AddWithValue("@tipo", obj.tipo);
                    cmd.Parameters.AddWithValue("@codUsario", obj.codUsario);
                    cmd.Parameters.AddWithValue("@monto", obj.monto);

                    //abrir la conexion
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultado = "1";
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return resultado;
            }
        }

        public string Delete(int key)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                var resultado = "0";
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_DELETE_INGRESO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idIngreso", key);

                    //abrir la conexion
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultado = "1";
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return resultado;
            }
        }

        public IngresoEntity GetById(int key)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                IngresoEntity oIngreso = new IngresoEntity();
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_GET_INGRESO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idIngreso", key);

                    //abrir la conexion
                    cmd.Connection.Open();

                    MySql.Data.MySqlClient.MySqlDataReader rd = cmd.ExecuteReader();
                    //ejecutar el query                    
                    while (rd.Read())
                    {
                        oIngreso.descripcion = rd.GetString("DESCRIPCION");
                        oIngreso.tipo = rd.GetString("TIPO");
                        oIngreso.fechaRegistro = rd.GetDateTime("FECHA_REGISTRO");
                        oIngreso.diaMesRegistro = rd.GetInt32("DIA_MES_INGRESO");
                        oIngreso.estado = rd.GetString("ESTADO");
                        oIngreso.monto = rd.GetDecimal("MONTO");
                        oIngreso.codUsario = rd.GetInt32("COD_USUARIO");
                        oIngreso.codMes = rd.GetString("COD_MES");
                        oIngreso.descripcionMes = rd.GetString("DESCRIPCION_MES");
                        oIngreso.fechaBaja = ClsCommon.getDate(rd["FECHA_BAJA"]);
                    }
                    return oIngreso;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }

        public List<IngresoEntity> GetByParameters(IngresoEntity obj)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                IngresoEntity oIngreso;
                var dtInit = new DateTime();
                List<IngresoEntity> listIngreso = new List<IngresoEntity>();
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_LIST_INGRESOS";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idUsuario", obj.codUsario);
                    if (obj.fechaDesde == dtInit) cmd.Parameters.AddWithValue("@fechaDesde", null);
                    else cmd.Parameters.AddWithValue("@fechaDesde", obj.fechaDesde);
                    if (obj.fechaHasta == dtInit) cmd.Parameters.AddWithValue("@fechaHasta", null);
                    else cmd.Parameters.AddWithValue("@fechaHasta", obj.fechaHasta);
                   
                    //abrir la conexion
                    cmd.Connection.Open();

                    MySql.Data.MySqlClient.MySqlDataAdapter adr = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
                    DataTable dtData = new DataTable();
                    adr.Fill(dtData);
                    foreach (DataRow dr in dtData.Rows) {
                        oIngreso = new IngresoEntity();
                        oIngreso.idIngreso = ClsCommon.getInt32(dr["ID_INGRESO"]);
                        oIngreso.descripcion = ClsCommon.getString(dr["DESCRIPCION"]);
                        oIngreso.tipo = ClsCommon.getString(dr["TIPO"]);
                        oIngreso.fechaRegistro = ClsCommon.getDate(dr["FECHA_REGISTRO"]);
                        oIngreso.diaMesRegistro = ClsCommon.getInt32(dr["DIA_MES_INGRESO"]);
                        oIngreso.estado = ClsCommon.getString(dr["ESTADO"]);
                        oIngreso.codUsario = ClsCommon.getInt32(dr["COD_USUARIO"]);
                        oIngreso.codMes = ClsCommon.getString(dr["COD_MES"]);
                        oIngreso.descripcionMes = ClsCommon.getString(dr["DESCRIPCION_MES"]);
                        oIngreso.fechaBaja = ClsCommon.getDate(dr["FECHA_BAJA"]);
                        listIngreso.Add(oIngreso);
                    }
                    return listIngreso;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }

        public string Update(IngresoEntity obj)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                IngresoEntity oIngreso = new IngresoEntity();
                var resultado = "0";
                try
                {
                    // setear parametros del command
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = ConexionDB.ConexionDBase();
                    cmd.CommandText = "pagodb.SP_UPDATE_INGRESO";

                    //asignar paramentros
                    cmd.Parameters.AddWithValue("@idIngreso", obj.idIngreso);
                    cmd.Parameters.AddWithValue("@descripcion", obj.descripcion);
                    cmd.Parameters.AddWithValue("@tipo", obj.tipo);
                    cmd.Parameters.AddWithValue("@monto", obj.monto);

                    //abrir la conexion
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultado = "1";
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return resultado;
            }
        }
    }
}
