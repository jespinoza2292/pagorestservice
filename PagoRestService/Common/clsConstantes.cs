﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PagoRestService.Common
{
    public class clsConstantes
    {
        public const string messageSuccessSave = "Se guardo el registro correctamente";
        public const string messageFailSave = "Ocurrio un error al guardar el registro";
        public const string messageSuccessUpdate = "Se actualizo el registro correctamente";
        public const string messageFailUpdate = "Ocurrio un error al actualizar el registro";
        public const string messageSuccessDelete = "Se elimino el registro correctamente";
        public const string messageFailDelete = "Ocurrio un error al elimino el registro";
        public const string messageSuccessConsulta = "Consulta Realizada Satisfactoriamente";
        public const string messageFailConsulta = "Ocurrio un error al realizar la consulta";
        public const string codeSuccess = "0";
        public const string codeFail = "1";
    }
}