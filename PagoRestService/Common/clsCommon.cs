﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PagoRestService.Common
{
    public class clsCommon
    {
        public static DateTime getDate(object attribute) {
            if (attribute != null)
            {
                if (attribute is DateTime)
                {
                    return Convert.ToDateTime(attribute);
                }
            }
            return new DateTime();
        }

        public static String returnMessageByResult(String result, String messageSucces, String messageFail) {
            if (result == clsConstantes.codeSuccess) return messageSucces;
            else return messageFail;
        }
    }
}