﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PagoRestService.Models
{
    public class ResponseBaseModel
    {
        public int codigoRespuesta { get; set; }
        public string mensajeRespuesta { get; set; }
    }
}