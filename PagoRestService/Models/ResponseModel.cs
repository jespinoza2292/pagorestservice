﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PagoRestService.Models
{
    public class ResponseModel<T> : ResponseBaseModel
    {
        public T body { get; set; }
    }
}