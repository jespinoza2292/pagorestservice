﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PagoRestService.Models.Request
{
    public class IngresoBodyRequest {
        public string descripcion { get; set; }
        public string tipo { get; set; }
        public decimal monto { get; set; }
    }

    public class IngresoUpdateRequest : IngresoBodyRequest
    {
        public int idIngreso { get; set; }
    }

    public class IngresoRequest : IngresoBodyRequest
    {
        public int codUsario { get; set; }
    }

    public class BusquedaIngresoRequest {
        public int codUsuario { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
    }
}