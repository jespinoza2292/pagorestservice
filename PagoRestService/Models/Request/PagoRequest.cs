﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PagoRestService.Models.Request
{

    public class PagoBodyRequest
    {
        public string descripcion { get; set; }
        public string tipo { get; set; }
        public decimal monto { get; set; }
    }

    public class PagoUpdateRequest : PagoBodyRequest
    {
        public int idPago { get; set; }
    }

    public class PagoRequest : PagoBodyRequest
    {
        public int codUsario { get; set; }
    }

    public class BusquedaPagoRequest
    {
        public int codUsuario { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
    }

}