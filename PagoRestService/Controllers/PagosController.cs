﻿using PagoDataAccess;
using PagoDataAccess.Entity;
using PagoRestService.Common;
using PagoRestService.Models;
using PagoRestService.Models.Request;
using PagoRestService.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static PagoRestService.Models.Request.PagoRequest;

namespace PagoRestService.Controllers
{
    [Authorize]
    [RoutePrefix("api/pagos")]
    public class PagosController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetId(int id)
        {
            try
            {
                var pagoBussinesss = new PagoBussiness();
                var pago = pagoBussinesss.GetById(id);

                var pagoReturn = new PagoResponse()
                {
                    idPago = id,
                    descripcion = pago.descripcion,
                    tipo = pago.tipo,
                    fechaRegistro = pago.fechaRegistro,
                    diaMesRegistro = pago.diaMesRegistro,
                    estado = pago.estado,
                    codUsario = pago.codUsario,
                    codMes = pago.codMes,
                    descripcionMes = pago.descripcionMes,
                    fechaBaja = pago.fechaBaja,
                    monto = pago.monto
                };
                var response = new ResponseModel<PagoResponse>();
                response.codigoRespuesta = Convert.ToInt32(clsConstantes.codeSuccess);
                response.mensajeRespuesta = clsCommon.returnMessageByResult(clsConstantes.codeSuccess, clsConstantes.messageSuccessConsulta, String.Empty);
                response.body = pagoReturn;
                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new ResponseModel<Exception>();
                response.codigoRespuesta = Convert.ToInt32(clsConstantes.codeFail);
                response.mensajeRespuesta = clsCommon.returnMessageByResult(clsConstantes.codeFail, String.Empty, clsConstantes.messageFailConsulta);
                response.body = ex;
                return Ok(response);
            }
        }

        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll(BusquedaPagoRequest pago)
        {
            try
            {
                var pagoBussinesss = new PagoBussiness();
                var pagoEntity = new PagoEntity()
                {
                    codUsario = pago.codUsuario,
                    fechaDesde = pago.fechaDesde,
                    fechaHasta = pago.fechaHasta
                };

                var listResult = pagoBussinesss.GetByParameters(pagoEntity);
                List<PagoResponse> listResponse = listResult.Select(p => new PagoResponse()
                {
                    idPago = p.idPago,
                    descripcion = p.descripcion,
                    tipo = p.tipo,
                    fechaRegistro = p.fechaRegistro,
                    diaMesRegistro = p.diaMesRegistro,
                    estado = p.estado,
                    codUsario = p.codUsario,
                    codMes = p.codMes,
                    descripcionMes = p.descripcionMes,
                    fechaBaja = p.fechaBaja,
                    monto = p.monto
                }).ToList();

                var response = new ResponseModel<List<PagoResponse>>();
                response.codigoRespuesta = Convert.ToInt32(clsConstantes.codeSuccess);
                response.mensajeRespuesta = clsCommon.returnMessageByResult(clsConstantes.codeSuccess, clsConstantes.messageSuccessConsulta, String.Empty);
                response.body = listResponse;
                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new ResponseModel<Exception>();
                response.codigoRespuesta = Convert.ToInt32(clsConstantes.codeFail);
                response.mensajeRespuesta = clsCommon.returnMessageByResult(clsConstantes.codeFail, String.Empty, clsConstantes.messageFailConsulta);
                response.body = ex;
                return Ok(response);
            }
        }

        [HttpPost]
        [Route("Save")]
        public IHttpActionResult Save(PagoRequest pago)
        {

            var pagosBussinesss = new PagoBussiness();
            var pagoEntity = new PagoEntity()
            {
                descripcion = pago.descripcion,
                monto = pago.monto,
                tipo = pago.tipo,
                codUsario = pago.codUsario
            };

            var resultado = pagosBussinesss.Create(pagoEntity);

            var response = new ResponseBaseModel();
            response.codigoRespuesta = Convert.ToInt32(resultado);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(resultado, clsConstantes.messageSuccessSave, clsConstantes.messageFailSave);
            return Ok(response);
        }

        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Update(PagoUpdateRequest pago)
        {
            var pagosBussinesss = new PagoBussiness();
            var pagoEntity = new PagoEntity()
            {
                idPago = pago.idPago,
                descripcion = pago.descripcion,
                monto = pago.monto,
                tipo = pago.tipo
            };

            var resultado = pagosBussinesss.Update(pagoEntity);

            var response = new ResponseBaseModel();
            response.codigoRespuesta = Convert.ToInt32(resultado);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(resultado, clsConstantes.messageSuccessUpdate, clsConstantes.messageFailUpdate);
            return Ok(response);
        }

        [HttpDelete]
        [Route("Delete")]
        public IHttpActionResult Delete(int id)
        {
            var pagosBussinesss = new PagoBussiness();
            var resultado = pagosBussinesss.Delete(id);
            var response = new ResponseBaseModel();
            response.codigoRespuesta = Convert.ToInt32(resultado);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(resultado, clsConstantes.messageSuccessDelete, clsConstantes.messageFailDelete);
            return Ok(response);
        }
    }
}
