﻿using PagoDataAccess;
using PagoDataAccess.Entity;
using PagoRestService.Common;
using PagoRestService.Models;
using PagoRestService.Models.Request;
using PagoRestService.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PagoRestService.Controllers
{
    [Authorize]
    [RoutePrefix("api/ingresos")]
    public class IngresosController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetId(int id)
        {
            var ingresosBussinesss = new IngresoBussiness();
            var ingreso = ingresosBussinesss.GetById(id);

            var ingresoReturn = new IngresoReponse() {
                idIngreso = id,
                descripcion = ingreso.descripcion,
                tipo = ingreso.tipo,
                fechaRegistro = ingreso.fechaRegistro,
                diaMesRegistro = ingreso.diaMesRegistro,
                estado = ingreso.estado,
                codUsario = ingreso.codUsario,
                codMes = ingreso.codMes,
                descripcionMes = ingreso.descripcionMes,
                fechaBaja = ingreso.fechaBaja,
                monto = ingreso.monto
            };

            var response = new ResponseModel<IngresoReponse>();
            response.codigoRespuesta = Convert.ToInt32(clsConstantes.codeSuccess);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(clsConstantes.codeSuccess, clsConstantes.messageSuccessConsulta, String.Empty);
            response.body = ingresoReturn;

            return Ok(response);
        }

        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll(BusquedaIngresoRequest ingreso)
        {
            var ingresosBussinesss = new IngresoBussiness();
            var ingresoEntity = new IngresoEntity()
            {
                codUsario = ingreso.codUsuario,
                fechaDesde = ingreso.fechaDesde,
                fechaHasta  = ingreso.fechaHasta
            };

            var listResult = ingresosBussinesss.GetByParameters(ingresoEntity);
            List<IngresoReponse> listResponse = listResult.Select(p => new IngresoReponse()
                                                  {
                                                      idIngreso = p.idIngreso,
                                                      descripcion = p.descripcion,
                                                      tipo = p.tipo,
                                                      fechaRegistro = p.fechaRegistro,
                                                      diaMesRegistro = p.diaMesRegistro,
                                                      estado = p.estado,
                                                      codUsario = p.codUsario,
                                                      codMes = p.codMes,
                                                      descripcionMes = p.descripcionMes,
                                                      fechaBaja = p.fechaBaja,
                                                      monto = p.monto
                                                  }).ToList();

            var response = new ResponseModel<List<IngresoReponse>>();
            response.codigoRespuesta = Convert.ToInt32(clsConstantes.codeSuccess);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(clsConstantes.codeSuccess, clsConstantes.messageSuccessConsulta, String.Empty);
            response.body = listResponse;
            return Ok(response);
        }

        [HttpDelete]
        [Route("Delete")]
        public IHttpActionResult Delete(int id) {
            var ingresosBussinesss = new IngresoBussiness();
            var resultado = ingresosBussinesss.Delete(id);
            var response = new ResponseBaseModel();
            response.codigoRespuesta = Convert.ToInt32(resultado);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(resultado, clsConstantes.messageSuccessDelete, clsConstantes.messageFailDelete);
            return Ok(response);
        }

        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Update(IngresoUpdateRequest ingreso) {
            var ingresosBussinesss = new IngresoBussiness();
            var ingresoEntity = new IngresoEntity()
            {
                idIngreso = ingreso.idIngreso,
                descripcion = ingreso.descripcion,
                monto = ingreso.monto,
                tipo = ingreso.tipo
            };

            var resultado = ingresosBussinesss.Update(ingresoEntity);

            var response = new ResponseBaseModel();
            response.codigoRespuesta = Convert.ToInt32(resultado);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(resultado, clsConstantes.messageSuccessUpdate, clsConstantes.messageFailUpdate);
            return Ok(response);
        }


        [HttpPost]
        [Route("Save")]
        public IHttpActionResult Save(IngresoRequest ingreso) {

            var ingresosBussinesss = new IngresoBussiness();
            var ingresoEntity = new IngresoEntity() {
                 descripcion = ingreso.descripcion,
                 monto = ingreso.monto,
                 tipo = ingreso.tipo,
                 codUsario = ingreso.codUsario
            };

            var resultado = ingresosBussinesss.Create(ingresoEntity);

            var response = new ResponseBaseModel();
            response.codigoRespuesta = Convert.ToInt32(resultado);
            response.mensajeRespuesta = clsCommon.returnMessageByResult(resultado, clsConstantes.messageSuccessSave, clsConstantes.messageFailSave);
            return Ok(response);
        }
    }
}
